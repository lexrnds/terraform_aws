#В данном шаблоне создаётся EC2 инстанс с Ubuntu Server 18.04 AMI, 4 vCPU, 4GB RAM и 50GB EBS HDD, 
#с открытыми портами 22 (SSH) и 443 (HTTPS). В качестве ключа SSH используется my-keypair.pem
#из домашней директории пользователя. Также создаётся security group для инстанса, с открытым доступом по SSH и HTTPS.
#После создания инстанса производится установка Apache2 web-сервера и настройка автозапуска при старте системы.


provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "example" {
  ami                         = "ami-0c55b159cbfafe1f0"
  instance_type               = "t2.medium"
  key_name                    = "my-keypair"
  associate_public_ip_address = true
  
  tags = {
    Name = "example-instance"
  }

  vpc_security_group_ids = [
    "${aws_security_group.instance.id}",
  ]

  root_block_device {
    volume_type           = "gp2"
    volume_size           = 50
  }

  ebs_block_device {
    device_name = "/dev/sdf"
    volume_type = "gp2"
    volume_size = 100
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("~/.ssh/my-keypair.pem")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y apache2",
      "sudo systemctl start apache2",
      "sudo systemctl enable apache2"
    ]
  }
}

resource "aws_security_group" "instance" {
  name_prefix = "instance_sg_"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}