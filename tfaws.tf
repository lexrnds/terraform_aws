# Параметры AWS
provider "aws" {
  region = "us-east-1" # Меняем регион на нужный
}

# Создаем EC2 instance
resource "aws_instance" "web" {
  ami           = "ami-0c94855ba95c71c99" # Ubuntu 20.04 AMI
  instance_type = "t3.medium" # Настраиваем тип EC2 инстанса
  vpc_security_group_ids = [aws_security_group.allow_all.id] # Подключаем ранее созданную Security Group
  user_data = <<-EOF
              #!/bin/bash
              apt-get update -y
              apt-get install nginx -y
              systemctl start nginx
              systemctl enable nginx
              EOF

  # Настройка атрибутов EC2 instance
  root_block_device {
    volume_size = 80 # Размер SSD
  }

  # Настраиваем теги EC2 instance
  tags = {
    Name = "test-app-ec2"
  }
}

# Создаем Security Group для доступа к 443 порту
resource "aws_security_group" "allow_https" {
  name_prefix = "allow_https"
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow_https"
  }
}

# Настройки Elastic Beanstalk
resource "aws_elastic_beanstalk_application" "test_app" {
  name = "test-app-eb"
}

resource "aws_elastic_beanstalk_environment" "test_app_env" {
  name = "test-app-env"
  application = "${aws_elastic_beanstalk_application.test_app.name}"
  solution_stack_name = "64bit Amazon Linux 2 v5.4.7 running Ruby 2.7"
}

# Конечный результат
output "public_ip" {
  value = "${aws_instance.web.public_ip}"
}

output "beanstalk_url" {
  value = "${aws_elastic_beanstalk_environment.test_app_env.endpoint_url}"
}